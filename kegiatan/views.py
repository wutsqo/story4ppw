from django.shortcuts import render, redirect
from django.http import HttpResponseBadRequest
from .models import Kegiatan, Peserta

def index(request):
    response = {
        'title' : 'Kegiatan',
        'daftar_kegiatan' : Kegiatan.objects.all()
    }
    return render(request, 'kegiatan/index.html', response)

def tambah_kegiatan(request):    
    if (request.method == 'POST'):
        try:
            kegiatan_baru = Kegiatan(name=request.POST['kegiatan_name'],deskripsi=request.POST['deskripsi'])
            kegiatan_baru.save()
        except:
            return HttpResponseBadRequest('Kegiatan instance not found')
    return redirect('kegiatan:index')

def tambah_peserta(request, pk=None):
    if (request.method == 'POST'):
        kegiatan = Kegiatan.objects.get(pk=pk)
        name = request.POST['nama_peserta']
        peserta = Peserta(name=name, kegiatan=kegiatan)
        peserta.save()
        return redirect(f'/kegiatan/peserta/{kegiatan.pk}/')
    else:
        return redirect('kegiatan:index')

def daftar_peserta(request, pk=None):
    response = {
        'title' : 'Detail Kegiatan',
        'kegiatan' : Kegiatan.objects.get(pk=pk)
    }
    return render(request, 'kegiatan/peserta.html', response)