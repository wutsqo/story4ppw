from django.test import TestCase, Client
from django.http import HttpRequest
from . import views
from .models import Kegiatan, Peserta

class KegiatanTest(TestCase):
    def test_apakah_pipeline_berjalan(self):
        self.assertEquals('p','p')

    def test_apakah_base_url_ada(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_template_base(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'base.html')

    def test_apakah_url_index_ada(self):
        response = Client().get('/kegiatan/')
        self.assertEquals(response.status_code, 200)

    def test_template_index(self):
        response = self.client.get('/kegiatan/')
        self.assertTemplateUsed(response, 'kegiatan/index.html')

    def test_get_tambah_kegiatan(self):
        response = Client().get('/kegiatan/tambah-kegiatan/')
        self.assertEquals(response.status_code, 302)
    
    def test_post_tambah_kegiatan_tanpa_data(self):
        response = Client().post('/kegiatan/tambah-kegiatan/')
        self.assertEquals(response.status_code, 400)
    
    def test_post_tambah_kegiatan_dengan_data(self):
        response = Client().post('/kegiatan/tambah-kegiatan/', data={'kegiatan_name':'nama kegiatan baru', 'deskripsi':'deskripsi baru'})
        self.assertEquals(response.status_code, 302)

    def test_views_index_response(self):
        response = views.index(HttpRequest())
        self.assertEquals(response.status_code, 200)

    def test_views_add_peserta(self):
        kegiatan_baru = Kegiatan(name='p')
        kegiatan_baru.save()
        request = HttpRequest()
        request.method = 'POST'
        request.POST['nama_peserta'] = 'peserta'
        response = views.tambah_peserta(request, kegiatan_baru.pk)
        self.assertEquals(response.status_code, 302)

    def test_views_add_peserta_get(self):
        response = Client().get('/kegiatan/tambah-kegiatan/')
        self.assertEquals(response.status_code, 302)
    
    def test_views_daftar_peserta(self):
        kegiatan = Kegiatan(name='p')
        kegiatan.save()
        response = views.daftar_peserta(HttpRequest(), kegiatan.pk)
        self.assertEquals(response.status_code, 200)

    def test_models_tambah_kegiatan(self):
        kegiatan_baru = Kegiatan(name='kegiatan baru', deskripsi='deskripsi kegiatan baru')
        kegiatan_baru.save()
        self.assertEquals(Kegiatan.objects.all().count(), 1)
    
    def test_models_tambah_peserta(self):
        kegiatan_baru = Kegiatan(name='kegiatan baru', deskripsi='deskripsi kegiatan baru')
        kegiatan_baru.save()
        peserta_baru = Peserta(kegiatan=kegiatan_baru, name='nama orang')
        peserta_baru.save()
        self.assertEquals(Peserta.objects.all().count(), 1)
    

