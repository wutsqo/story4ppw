from django.shortcuts import render, redirect
from .models import MataKuliah
from .forms import FormMataKuliah

def home(request):
    semua_matkul = MataKuliah.objects.all()
    response = {
        'title' : 'List Mata Kuliah',
        'semua_matkul' : semua_matkul
    }

    return render(request, 'matkul/home.html', response)

def tambah(request):
    form_tambah = FormMataKuliah(request.POST or None)

    if request.method == 'POST':
        if form_tambah.is_valid():
            form_tambah.save()
            return redirect('matkul:home')

    response = {
        'title' : 'Tambah Mata Kuliah Baru',
        'form_tambah' : form_tambah
    }

    return render(request, 'matkul/new.html', response)

def detail(request, delete_id):
    detail_matkul = MataKuliah.objects.get(id=delete_id)
    response = {
        'title' : 'Detail Mata Kuliah',
        'detail_matkul' : detail_matkul
    }

    return render(request, 'matkul/detail.html', response)

def hapus(request, delete_id):
    MataKuliah.objects.filter(id = delete_id).delete()
    return redirect('matkul:home')