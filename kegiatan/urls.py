from django.urls import path

from . import views

app_name = 'kegiatan'

urlpatterns = [
    path('', views.index, name='index'),
    path('tambah-kegiatan/', views.tambah_kegiatan, name='tambah_kegiatan'),
    path('tambah-peserta/<int:pk>/', views.tambah_peserta, name='tambah_peserta'),
    path('peserta/<int:pk>/', views.daftar_peserta, name='daftar_peserta')
]
