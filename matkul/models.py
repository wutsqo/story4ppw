from django.db import models

class MataKuliah(models.Model):
    nama = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    sks = models.IntegerField()
    desc = models.CharField(max_length=100)
    semester = models.CharField(max_length=15)
    kelas = models.CharField(max_length=15)

    def __str__(self):
        return "{}.{}".format(self.id, self.nama)