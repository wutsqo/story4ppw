from django import forms
from .models import MataKuliah

class FormMataKuliah(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = [
            'nama',
            'dosen',
            'sks',
            'desc',
            'semester',
            'kelas'
        ]

    