from django.urls import path

from . import views

app_name = 'matkul'

urlpatterns = [
    path('', views.home, name='home'),
    path('tambah', views.tambah, name='tambah'),
    path('hapus/<int:delete_id>', views.hapus, name='hapus'),
    path('detail/<int:delete_id>', views.detail, name='detail')
]
